﻿using System;
using CodeChallenge.Tasks.Application;

namespace CodeChallenge.Tasks.Infrastructure
{
    public class SystemClock: ISystemClock
    {
        public DateTimeOffset UtcNow => DateTime.UtcNow;
    }
}
