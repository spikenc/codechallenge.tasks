﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using CodeChallenge.Tasks.Application.AuthenticationFeature;
using CodeChallenge.Tasks.Domain;
using CodeChallenge.Tasks.Application;
using System;

namespace CodeChallenge.Tasks.Infrastructure.Authentication
{
    public class TokenGenerator : ITokenGenerator
    {
        private readonly string _secret;
        private readonly ISystemClock _systemClock;

        public TokenGenerator(
            string secret,
            ISystemClock systemClock)
        {
            _secret = secret;
            _systemClock = systemClock;
        }

        public string GenerateToken(User user, TimeSpan expiresAfer)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("UserId", user.UserId.ToString())
                }),
                Expires = _systemClock.UtcNow.Add(expiresAfer).DateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
