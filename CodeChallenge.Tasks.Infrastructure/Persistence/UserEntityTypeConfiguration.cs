﻿using CodeChallenge.Tasks.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CodeChallenge.Tasks.Infrastructure.Persistence
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(x => x.UserId);
            builder.HasAlternateKey(x => x.Username);

            builder
                .Property(x => x.Username)
                .HasMaxLength(512);
            builder.Property(x => x.Name)
                .HasMaxLength(512);
        }
    }
}
