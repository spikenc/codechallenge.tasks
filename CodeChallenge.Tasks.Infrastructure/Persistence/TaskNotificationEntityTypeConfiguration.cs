﻿using CodeChallenge.Tasks.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CodeChallenge.Tasks.Infrastructure.Persistence
{
    public class TaskNotificationEntityTypeConfiguration : IEntityTypeConfiguration<TaskNotification>
    {
        public void Configure(EntityTypeBuilder<TaskNotification> builder)
        {
            builder.ToTable("TaskNotifications");

            builder.HasKey(x => x.TaskId);
        }
    }
}
