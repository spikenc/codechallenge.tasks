﻿using CodeChallenge.Tasks.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CodeChallenge.Tasks.Infrastructure.Persistence
{
    public class TaskEntityTypeConfiguration : IEntityTypeConfiguration<Task>
    {

        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.ToTable("Tasks");

            builder.HasKey(x => x.TaskId);
            builder
                .Property(x => x.Summary)
                .IsRequired()
                .HasMaxLength(2500);
            builder
                .Property(x => x.TaskOwnerId)
                .IsRequired();
        }
    }
}
