﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace CodeChallenge.Tasks.Infrastructure.Persistence
{
    public class TaskDbDesignTimeDbContextFactory
    : IDesignTimeDbContextFactory<TaskDbContext>
    {
        public TaskDbContext CreateDbContext(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<TaskDbContext>();
            optionsBuilder.UseMySQL(
                configuration.GetConnectionString("TaskDatabase"));
            return new TaskDbContext(optionsBuilder.Options);
        }
    }
}
