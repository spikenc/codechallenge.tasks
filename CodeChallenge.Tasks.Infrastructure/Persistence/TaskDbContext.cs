﻿using Microsoft.EntityFrameworkCore;
using CodeChallenge.Tasks.Domain;

namespace CodeChallenge.Tasks.Infrastructure.Persistence
{
    public class TaskDbContext: DbContext
    {
        public const string DatabaseSchema = "Tasks";

        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskNotification> TaskNotifications { get; set; }


        public TaskDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema(DatabaseSchema);
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaskEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaskNotificationEntityTypeConfiguration());
        }
    }
}
