﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.UserFeature;
using CodeChallenge.Tasks.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;

namespace CodeChallenge.Tasks.Infrastructure.Repositories
{
    public class UserRepository: IUserRepository
    {
        private readonly TaskDbContext _dbContext;

        public UserRepository(TaskDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Guid> CreateUser(Domain.User user)
        {
            _dbContext.Users.Add(user);
            await _dbContext.SaveChangesAsync();
            return user.UserId;
        }

        public async Task DeleteUser(Guid userId)
        {
            var user = await _dbContext.Users.SingleAsync(x => x.UserId == userId);
            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Domain.User> GetUserBy(string username, string password)
        {
            return await _dbContext.Users
                .Where(x =>
                    x.Username == username &&
                    x.PasswordHash == password)
                .SingleOrDefaultAsync();
        }

        public async Task<Domain.User> GetUserById(Guid userId)
        {
            return await _dbContext.Users.SingleOrDefaultAsync(x => x.UserId == userId);
        }

        public async Task<IEnumerable<Domain.User>> GetUsers()
        {
            return await _dbContext.Users.ToArrayAsync();
        }

        public async Task UpdateUser(Domain.User user)
        {
            var domain = await _dbContext.Users.SingleAsync(x => x.UserId == user.UserId);

            domain.Name = user.Name;

            await _dbContext.SaveChangesAsync();
        }
    }
}
