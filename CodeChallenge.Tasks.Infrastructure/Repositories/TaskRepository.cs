﻿using System;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.TaskFeature;
using CodeChallenge.Tasks.Infrastructure.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CodeChallenge.Tasks.Infrastructure.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly TaskDbContext _dbContext;

        public TaskRepository(TaskDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task CreateTask(Domain.Task task)
        {
            _dbContext.Tasks.Add(task);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteTask(Guid taskId)
        {
            var task = await _dbContext.Tasks.SingleAsync(x => x.TaskId == taskId);
            _dbContext.Tasks.Remove(task);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateTask(Domain.Task task)
        {
            var domain = await _dbContext.Tasks.SingleAsync(x => x.TaskId == task.TaskId);
            domain.Summary = task.Summary;
            domain.CompletedAt = task.CompletedAt;

            await _dbContext.SaveChangesAsync();
        }

        public async Task<Domain.Task> GetTaskById(Guid taskId)
        {
            return await _dbContext.Tasks.SingleOrDefaultAsync(x => x.TaskId == taskId);
        }

        public async Task<IEnumerable<Domain.Task>> GetTasksBy(Guid? requestorId, int page, int itemsPerPage)
        {
            var query = _dbContext.Tasks.AsQueryable();
            if (requestorId.HasValue) query = query.Where(x => x.TaskOwnerId == requestorId.Value);

            return await query
                .Skip(page * itemsPerPage)
                .Take(itemsPerPage)
                .ToArrayAsync();
        }
    }
}
