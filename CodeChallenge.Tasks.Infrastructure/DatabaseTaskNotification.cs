﻿using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.TaskNotificationFeature;
using CodeChallenge.Tasks.Infrastructure.Persistence;

namespace CodeChallenge.Tasks.Infrastructure
{
    public class DatabaseTaskNotification : ITaskNotification
    {
        private readonly TaskDbContext _dbContext;

        public DatabaseTaskNotification(TaskDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task SendNotification(Domain.TaskNotification notification)
        {
            _dbContext.TaskNotifications.Add(notification);
            await _dbContext.SaveChangesAsync();
        }
    }
}
