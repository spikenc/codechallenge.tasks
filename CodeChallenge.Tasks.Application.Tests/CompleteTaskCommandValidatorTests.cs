﻿using System;
using CodeChallenge.Tasks.Application.TaskFeature;
using FluentAssertions;
using Xunit;

namespace CodeChallenge.Tasks.Application.Tests
{
    public class CompleteTaskCommandValidatorTests
    {
        [Fact]
        public void CompleteTaskCommand_WhenUserIsTaskOwner_IsSuccessful()
        {
            var userId = Guid.NewGuid();
            var user = new Domain.User
            {
                UserId = userId,
                Role = Domain.UserRole.Technician
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = userId
            };

            var sut = new CompleteTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeTrue();

        }

        [Fact]
        public void CompleteTaskCommand_WhenUserIsNotTaskOwner_IsNotSuccessful()
        {
            var user = new Domain.User
            {
                UserId = Guid.NewGuid(),
                Role = Domain.UserRole.Manager
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = Guid.NewGuid()
            };

            var sut = new CompleteTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeFalse();
        }

        [Fact]
        public void CompleteTaskCommand_WhenTaskIsCompleted_IsNotSuccessful()
        {
            var userId = Guid.NewGuid();
            var user = new Domain.User
            {
                UserId = userId,
                Role = Domain.UserRole.Technician
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = userId,
                IsCompleted = true,
                CompletedAt = DateTime.UtcNow
            };

            var sut = new CompleteTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeFalse();
        }

        [Fact]
        public void CompleteTaskCommand_WhenUserIsManager_IsNotSuccessful()
        {
            var userId = Guid.NewGuid();
            var user = new Domain.User
            {
                UserId = userId,
                Role = Domain.UserRole.Manager
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = userId,
                IsCompleted = true,
                CompletedAt = DateTime.UtcNow
            };

            var sut = new CompleteTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeFalse();

        }
    }
}
            //        if (task.TaskOwnerId != user.UserId) return result.Fail("Invalid Operation");
            //if (task.CompletedAt != null) return result.Fail("Task is already completed");
