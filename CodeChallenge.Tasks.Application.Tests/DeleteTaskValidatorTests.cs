﻿using System;
using CodeChallenge.Tasks.Application.TaskFeature;
using FluentAssertions;
using Xunit;

namespace CodeChallenge.Tasks.Application.Tests
{
    public class DeleteTaskValidatorTests
    {
        [Fact]
        public void DeleteTask_WhenUserIsManager_IsSuccessful()
        {
            var user = new Domain.User
            {

                Role = Domain.UserRole.Manager
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = Guid.NewGuid()
            };

            var sut = new DeleteTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void DeleteTask_WhenUserIsTechnicial_IsNotSuccessful()
        {
            var user = new Domain.User
            {

                Role = Domain.UserRole.Technician
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = Guid.NewGuid()
            };

            var sut = new DeleteTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeFalse();
        }

        [Fact]
        public void DeleteTask_WhenUserNotExits_IsNotSuccessful()
        {
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = Guid.NewGuid()
            };

            var sut = new DeleteTaskCommandValidator(task, null);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeFalse();
        }

        [Fact]
        public void DeleteTask_WhenTaskNotExits_IsNotSuccessful()
        {
            var user = new Domain.User
            {

                Role = Domain.UserRole.Technician
            };

            var sut = new DeleteTaskCommandValidator(null, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeFalse();
        }
    }
}
