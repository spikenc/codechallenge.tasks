﻿using System;
using CodeChallenge.Tasks.Application.TaskFeature;
using Xunit;
using FluentAssertions;

namespace CodeChallenge.Tasks.Application.Tests
{
    public class CreateTaskValidatorTests
    {
        [Fact]
        public void CreateTaskCommandValidator_WhenUserIsManager_IsNotSuccessful()
        {
            var user = new Domain.User
            {

                Role = Domain.UserRole.Manager
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = Guid.NewGuid()
            };

            var sut = new CreateTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeFalse();
        }

        [Fact]
        public void CreateTaskCommandValidator_WhenUserIsTechnician_IsSuccessful()
        {
            var user = new Domain.User
            {

                Role = Domain.UserRole.Technician
            };
            var task = new Domain.Task
            {
                Summary = "A",
                TaskOwnerId = Guid.NewGuid()
            };

            var sut = new CreateTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().BeTrue();
        }

        [Fact]
        public void CreateTaskCommandValidator_WhenSummaryIsEmpty_IsNotSuccessful()
        {
            TestSummary(string.Empty, false);
        }

        [Fact]
        public void CreateTaskCommandValidator_WhenSummaryIsNull_IsNotSuccessful()
        {
            TestSummary(null, false);
        }

        [Fact]
        public void CreateTaskCommandValidator_WhenSummaryExceedsLength_IsNotSuccessful()
        {
            var summary = new string('a', 2501);
            TestSummary(summary, false);
        }

        [Fact]
        public void CreateTaskCommandValidator_WhenSummaryHasLessThanMaxLength_IsNotSuccessful()
        {
            var summary = new string('a', 40);
            TestSummary(summary, true);
        }

        private static void TestSummary(string value, bool expected)
        {
            var user = new Domain.User
            {

                Role = Domain.UserRole.Technician
            };
            var task = new Domain.Task
            {
                Summary = value,
                TaskOwnerId = Guid.NewGuid()
            };

            var sut = new CreateTaskCommandValidator(task, user);
            var result = sut.Validate();

            result.IsSuccessful.Should().Be(expected);
        }
    }
}
