﻿using System;
namespace CodeChallenge.Tasks.Application
{
    public interface ISystemClock
    {
        public DateTimeOffset UtcNow { get; }
    }
}
