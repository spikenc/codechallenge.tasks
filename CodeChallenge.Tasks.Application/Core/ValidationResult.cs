﻿using System;
namespace CodeChallenge.Tasks.Application.Core
{
    public class ValidationResult
    {
        public bool IsSuccessful { get; set; }
        public string Error { get; set; }
        public ValidationResult Success()
        {
            IsSuccessful = true;
            Error = string.Empty;
            return this;
        }
        public ValidationResult Fail(string error)
        {
            IsSuccessful = false;
            Error = error;
            return this;
        }
    }
}
