﻿namespace CodeChallenge.Tasks.Application.Core
{
    public class CommandResult<T>
    {
        public ValidationResult ValidationResult { get; }
        public T Result { get; set; }

        public CommandResult(ValidationResult result)
        {
            ValidationResult = result;
        }
    }
}