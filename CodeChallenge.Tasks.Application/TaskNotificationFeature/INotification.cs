﻿using System.Threading.Tasks;

namespace CodeChallenge.Tasks.Application.TaskNotificationFeature
{
    public interface ITaskNotification
    {
        Task SendNotification(Domain.TaskNotification notification);
    }
}
