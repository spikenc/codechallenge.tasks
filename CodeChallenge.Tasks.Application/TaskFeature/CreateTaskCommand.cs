﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class CreateTaskCommand: IRequest<CommandResult<Guid>>
    {
        public Guid TaskCreatorId { get; set; }
        public string Summary { get; set; }
    }

    public class CreateTaskResponse
    {
        public Guid? TaskId { get; set; }
    }

    public class CreateTaskCommandHandler : IRequestHandler<CreateTaskCommand, CommandResult<Guid>>    
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;

        public CreateTaskCommandHandler(
            ITaskRepository taskRepository,
            IUserRepository userRepository)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
        }

        public async Task<CommandResult<Guid>> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
        {
            var taskCreator = await _userRepository.GetUserById(request.TaskCreatorId);
            var task = new Domain.Task
            {
                TaskId = Guid.NewGuid(),
                Summary = request.Summary,
                TaskOwnerId = request.TaskCreatorId,
                CompletedAt = default,
                IsCompleted = false
            };

            var validator = new CreateTaskCommandValidator(task, taskCreator).Validate();
            var result = new CommandResult<Guid>(validator);
            if (!validator.IsSuccessful) return result;
           

            await _taskRepository.CreateTask(task);
            result.Result = task.TaskId;
            return result;
        }
    }
}
