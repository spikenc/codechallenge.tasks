﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class UpdateTaskCommand : IRequest<CommandResult<Domain.Task>>
    {
        public Guid TaskId { get; set; }
        public Guid Requestor { get; set; }
        public string Summary { get; set; }
    }

    public class UpdateTaskCommandHandler : IRequestHandler<UpdateTaskCommand, CommandResult<Domain.Task>>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISystemClock _systemClock;

        public UpdateTaskCommandHandler(
            ITaskRepository taskRepository,
            IUserRepository userRepository,
            ISystemClock systemClock)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _systemClock = systemClock;
        }

        public async Task<CommandResult<Domain.Task>> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await _taskRepository.GetTaskById(request.TaskId);
            var requestor = await _userRepository.GetUserById(request.Requestor);

            var validator = new UpdateTaskCommandValidator(task, requestor).Validate();
            var result = new CommandResult<Domain.Task>(validator);
            if (!validator.IsSuccessful) return result;

            task.Summary = request.Summary;
            await _taskRepository.UpdateTask(task);

            result.Result = task;
            return result;
        }
    }
}