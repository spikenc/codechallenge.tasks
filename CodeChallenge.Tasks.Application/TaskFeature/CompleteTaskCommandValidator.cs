﻿using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Domain;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class CompleteTaskCommandValidator
    {
        private readonly Task task;
        private readonly User user;

        public CompleteTaskCommandValidator(
            Domain.Task task,
            Domain.User user)
        {
            this.task = task;
            this.user = user;
        }
        public ValidationResult Validate()
        {
            var result = new ValidationResult();

            if (task.TaskOwnerId != user.UserId) return result.Fail("Invalid operation");
            if (task.CompletedAt != null) return result.Fail("Task is already completed");
            if (user.Role != UserRole.Technician) return result.Fail("Invalid role");

            return result.Success();
        }
    }
}
