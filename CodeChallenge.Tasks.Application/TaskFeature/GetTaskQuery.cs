﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class GetTaskQuery: IRequest<Domain.Task>
    {
        public Guid TaskId { get; set; }
        public Guid RequestorId { get; set; }
    }

    public class GetTaskQueryHandler : IRequestHandler<GetTaskQuery, Domain.Task>
    {
        private readonly ITaskRepository _taskRepository;

        public GetTaskQueryHandler(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public async Task<Domain.Task> Handle(GetTaskQuery request, CancellationToken cancellationToken)
        {
            var task = await _taskRepository.GetTaskById(request.TaskId);
            if (task.TaskOwnerId != request.RequestorId) return null;
            return task;
        }
    }
}
