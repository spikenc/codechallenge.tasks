﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public interface ITaskRepository
    {
        Task CreateTask(Domain.Task task);
        Task UpdateTask(Domain.Task task);
        Task DeleteTask(Guid taskId);
        Task<Domain.Task> GetTaskById(Guid taskId);
        Task<IEnumerable<Domain.Task>> GetTasksBy(Guid? requestorId, int page, int itemsPerPage);
    }
}
