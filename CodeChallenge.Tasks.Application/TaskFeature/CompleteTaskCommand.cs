﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Application.TaskNotificationFeature;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class CompleteTaskCommand : IRequest<CommandResult<Domain.Task>>
    {
        public Guid TaskId { get; set; }
        public Guid UserId { get; set; }
    }

    public class CompleTaskCommandHandler : IRequestHandler<CompleteTaskCommand, CommandResult<Domain.Task>>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISystemClock _systemClock;
        private readonly ITaskNotification _notification;

        public CompleTaskCommandHandler(
            ITaskRepository taskRepository,
            IUserRepository userRepository,
            ISystemClock systemClock,
            ITaskNotification notification)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _systemClock = systemClock;
            _notification = notification;
        }

        public async Task<CommandResult<Domain.Task>> Handle(CompleteTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await _taskRepository.GetTaskById(request.TaskId);
            var user = await _userRepository.GetUserById(request.UserId);


            var validator = new CompleteTaskCommandValidator(task, user).Validate();
            var result = new CommandResult<Domain.Task>(validator);
            if (!validator.IsSuccessful) return result;

            task.CompletedAt = _systemClock.UtcNow.DateTime;
            task.IsCompleted = true;
            await _taskRepository.UpdateTask(task);
            await _notification.SendNotification(new Domain.TaskNotification
            {
                TaskId = task.TaskId,
                CompletedAt = task.CompletedAt.Value
            });

            result.Result = task;
            return result;
        }
    }
}

