﻿using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Domain;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class DeleteTaskCommandValidator
    {
        private readonly Task _task;
        private readonly User _requestor;

        public DeleteTaskCommandValidator(
            Domain.Task task,
            Domain.User requestor)
        {
            _task = task;
            _requestor = requestor;
        }
        public ValidationResult Validate()
        {
            var result = new ValidationResult();

            if (_task == null) return result.Fail("Task does not exists");

            if (_requestor == null) return result.Fail("Requestor does not exist");
            if (_requestor.Role != Domain.UserRole.Manager) return result.Fail("Invalid Role");

            return result.Success();
        }
    }
}
