﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class GetTasksQuery: IRequest<IEnumerable<Domain.Task>>
    {
        public Guid RequestorUserId { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
    }

    public class GetTasksQueryHandler : IRequestHandler<GetTasksQuery, IEnumerable<Domain.Task>>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;

        public GetTasksQueryHandler(
            ITaskRepository taskRepository,
            IUserRepository userRepository)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<Domain.Task>> Handle(GetTasksQuery request, CancellationToken cancellationToken)
        {
            var requestor = await _userRepository.GetUserById(request.RequestorUserId);
            Guid? requestorId = (requestor.Role != Domain.UserRole.Manager) ? requestor.UserId : default(Guid?);
            return await _taskRepository.GetTasksBy(requestorId, request.Page, request.ItemsPerPage);
        }
    }
}
