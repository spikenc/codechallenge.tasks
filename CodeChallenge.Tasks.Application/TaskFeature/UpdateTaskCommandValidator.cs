﻿using System;
using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Domain;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class UpdateTaskCommandValidator
    {
        private const int SummaryMaxLength = 2500;
        private readonly Task _task;
        private readonly User _taskCreator;

        public UpdateTaskCommandValidator(
            Domain.Task task,
            Domain.User taskCreator)
        {
            _task = task;
            _taskCreator = taskCreator;
        }
        public ValidationResult Validate()
        {
            var result = new ValidationResult();

            if (_taskCreator == null) return result.Fail("Requestor does not exist");
            if (_taskCreator.Role != Domain.UserRole.Technician) return result.Fail("Invalid role");

            if (_task.IsCompleted) return result.Fail("Task is already completed");
            if (_task.TaskOwnerId != _taskCreator.UserId) return result.Fail("Requestor is not the task owner");
            

            if (string.IsNullOrEmpty(_task.Summary)) return result.Fail("Summary cannot be empty");
            if (_task.Summary.Length > SummaryMaxLength) return result.Fail("Summary exceeded max length");


            return result.Success();
        }
    }
}
