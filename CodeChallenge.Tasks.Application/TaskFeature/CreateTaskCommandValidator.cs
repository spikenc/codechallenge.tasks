﻿using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Domain;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class CreateTaskCommandValidator
    {
        private const int SummaryMaxLength = 2500;
        private readonly Task _task;
        private readonly User _taskCreator;

        public CreateTaskCommandValidator(
            Domain.Task task,
            Domain.User taskCreator)
        {
            _task = task;
            _taskCreator = taskCreator;
        }

        public ValidationResult Validate()
        {
            var result = new ValidationResult();

            if (_taskCreator == null) return result.Fail("Requestor does not exist");
            if (_taskCreator.Role == Domain.UserRole.Manager) return result.Fail("Invalid Role");

            if (string.IsNullOrEmpty(_task.Summary)) return result.Fail("Summary cannot be empty");
            if (_task.Summary.Length > SummaryMaxLength) return result.Fail("Summary exceeded max length");

            return result.Success();
        }
    }
}