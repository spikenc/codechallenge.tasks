﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;

namespace CodeChallenge.Tasks.Application.TaskFeature
{
    public class DeleteTaskCommand: IRequest<CommandResult<Guid>>
    {
        public Guid TaskId { get; set; }
        public Guid RequestorUserId { get; set; }
    }

    public class DeleteTaskCommandHandler : IRequestHandler<DeleteTaskCommand, CommandResult<Guid>>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUserRepository _userRepository;

        public DeleteTaskCommandHandler(
            ITaskRepository taskRepository,
            IUserRepository userRepository)
        {
            _taskRepository = taskRepository;
            _userRepository = userRepository;
        }

        public async Task<CommandResult<Guid>> Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await _taskRepository.GetTaskById(request.TaskId);
            var requestor = await _userRepository.GetUserById(request.RequestorUserId);

            var validator = new DeleteTaskCommandValidator(task, requestor).Validate();
            var result = new CommandResult<Guid>(validator);
            if (!validator.IsSuccessful) return result;

            await _taskRepository.DeleteTask(request.TaskId);

            result.Result = request.TaskId;
            return result;
        }
    }
}
