﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.Core;
using CodeChallenge.Tasks.Domain;
using MediatR;

namespace CodeChallenge.Tasks.Application.UserFeature
{
    public class CreateUserCommand : IRequest<CommandResult<Guid>>
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }
    }

    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, CommandResult<Guid>>
    {
        private readonly IUserRepository _userRepository;

        public CreateUserCommandHandler(
            IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<CommandResult<Guid>> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var validator = new CreateUserCommandValidator(request).Validate();
            var result = new CommandResult<Guid>(validator);
            if (!validator.IsSuccessful) return result;

            var domain = new Domain.User
            {
                UserId = Guid.NewGuid(),
                Name = request.Name,
                Username = request.UserName,
                PasswordHash = request.Password,
                Role = request.Role,
            };
            await _userRepository.CreateUser(domain);
            result.Result = domain.UserId;
            return result;
        }
    }
}
