﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Domain;
using MediatR;

namespace CodeChallenge.Tasks.Application.UserFeature
{

    public class GetUsersByIdQuery : IRequest<Domain.User>
    {
        public Guid UserId { get; set; }
    }

    public class GetUsersByIdQueryHandler : IRequestHandler<GetUsersByIdQuery, Domain.User>
    {
        private readonly IUserRepository _userRepository;

        public GetUsersByIdQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> Handle(GetUsersByIdQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.GetUserById(request.UserId);
        }
    }
}
