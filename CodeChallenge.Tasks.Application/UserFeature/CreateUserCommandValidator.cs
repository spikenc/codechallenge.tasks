﻿using CodeChallenge.Tasks.Application.Core;

namespace CodeChallenge.Tasks.Application.UserFeature
{
    public class CreateUserCommandValidator
    {
        private readonly CreateUserCommand request;

        public CreateUserCommandValidator(
            CreateUserCommand request)
        {
            this.request = request;
        }
        public ValidationResult Validate()
        {
            var result = new ValidationResult();

            if (string.IsNullOrEmpty(request.Name)) return result.Fail("Invalid Name");
            if (string.IsNullOrEmpty(request.UserName)) return result.Fail("Invalid Username");
            if (string.IsNullOrEmpty(request.Password)) return result.Fail("Invalid Password");

            return result.Success();
        }
    }
}
