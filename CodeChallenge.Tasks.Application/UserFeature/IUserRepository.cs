﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeChallenge.Tasks.Application.UserFeature
{
    public interface IUserRepository
    {
        Task<Guid> CreateUser(Domain.User user);
        Task UpdateUser(Domain.User user);
        Task DeleteUser(Guid userId);
        Task<Domain.User> GetUserById(Guid userId);
        Task<Domain.User> GetUserBy(string username, string password);
        Task<IEnumerable<Domain.User>> GetUsers();
    }
}
