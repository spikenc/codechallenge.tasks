﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Domain;
using MediatR;

namespace CodeChallenge.Tasks.Application.UserFeature
{
    public class GetUsersQuery : IRequest<IEnumerable<Domain.User>>
    {
    }

    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, IEnumerable<Domain.User>>
    {
        private readonly IUserRepository _userRepository;

        public GetUsersQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<User>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.GetUsers();
        }
    }
}
