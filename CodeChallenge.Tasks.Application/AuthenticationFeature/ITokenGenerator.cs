﻿using System;
namespace CodeChallenge.Tasks.Application.AuthenticationFeature
{
    public interface ITokenGenerator
    {
        string GenerateToken(Domain.User user, TimeSpan expiresAfer);
    }
}
