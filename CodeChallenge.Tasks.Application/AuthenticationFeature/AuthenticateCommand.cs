﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.AuthenticationFeature;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;

namespace CodeChallenge.Tasks.Application.Authentication
{
    public class AuthenticateCommand : IRequest<AuthenticateResponse>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class AuthenticateResponse
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }

    }

    public class AuthenticateCommandHandler : IRequestHandler<AuthenticateCommand, AuthenticateResponse>
    {
        private readonly IUserRepository _userRepository;
        private readonly ITokenGenerator _tokenGenerator;

        public AuthenticateCommandHandler(
            IUserRepository userRepository,
            ITokenGenerator tokenGenerator)
        {
            _userRepository = userRepository;
            _tokenGenerator = tokenGenerator;
        }

        public async Task<AuthenticateResponse> Handle(AuthenticateCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetUserBy(request.Username, request.Password);
            if (user == null) return null;

            var response = new AuthenticateResponse
            {
                UserId = user.UserId,
                Token = _tokenGenerator.GenerateToken(user, TimeSpan.FromHours(1))
            };
            return response;
        }
    }
}
