﻿using System;
namespace CodeChallenge.Tasks.Domain
{
    public enum UserRole
    {
        Technician,
        Manager
    }
}
