﻿using System;
namespace CodeChallenge.Tasks.Domain
{
    public class User
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public UserRole Role { get; set; }
        public string PasswordHash { get; set; }
    }
}
