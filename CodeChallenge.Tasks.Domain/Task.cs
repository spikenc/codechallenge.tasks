﻿using System;
namespace CodeChallenge.Tasks.Domain
{
    public class Task
    {
        public Guid TaskId { get; set; }
        public string Summary { get; set; }
        public Guid TaskOwnerId { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime? CompletedAt { get; set; }
    }
}
