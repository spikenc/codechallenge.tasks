﻿using System;
namespace CodeChallenge.Tasks.Domain
{
    public class TaskNotification
    {
        public Guid TaskId { get; set; }
        public DateTime CompletedAt { get; set; }
    }
}
