﻿using CodeChallenge.Tasks.Infrastructure.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MediatR;
using System.Reflection;
using CodeChallenge.Tasks.Infrastructure;
using CodeChallenge.Tasks.Application;
using CodeChallenge.Tasks.Application.TaskFeature;
using CodeChallenge.Tasks.Infrastructure.Repositories;
using CodeChallenge.Tasks.Application.UserFeature;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using CodeChallenge.Tasks.Application.AuthenticationFeature;
using CodeChallenge.Tasks.Infrastructure.Authentication;
using CodeChallenge.Tasks.Application.TaskNotificationFeature;

namespace CodeChallenge.Tasks.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAuthorization();
            services.AddDbContext<TaskDbContext>(
                options => options.UseMySQL(Configuration.GetConnectionString("TaskDatabase")),
                ServiceLifetime.Transient);

            services.AddSingleton<ISystemClock, SystemClock>();
            services.AddTransient<ITaskRepository, TaskRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ITaskNotification, DatabaseTaskNotification>();
            services.AddMediatR(typeof(CreateTaskCommand).GetTypeInfo().Assembly);

            var secret = Configuration.GetValue<string>("secret");
            services.AddSingleton<ITokenGenerator, TokenGenerator>(x =>
            {
                var clock = x.GetRequiredService<ISystemClock>();
                return new TokenGenerator(secret, clock);
            });
            AddAuthentication(services, secret);
            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();


            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllers()
                    .RequireAuthorization();
            });

            using (var scope = app.ApplicationServices.CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<TaskDbContext>()
                    .Database
                    .Migrate();
            }
        }

        private static void AddAuthentication(IServiceCollection services, string secret)
        {
            services
            .AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }
    }
}
