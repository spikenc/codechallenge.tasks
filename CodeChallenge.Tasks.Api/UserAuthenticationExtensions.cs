﻿using System;
using System.Security.Claims;
using System.Linq;

namespace CodeChallenge.Tasks.Api
{
    public static class UserAuthenticationExtensions
    {
        public static Guid GetUserId(this ClaimsPrincipal data)
        {
            return Guid.Parse(data.Claims.First(x => x.Type == "UserId").Value);
        }
    }
}
