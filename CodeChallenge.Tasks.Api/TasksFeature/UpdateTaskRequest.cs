﻿using System;
namespace CodeChallenge.Tasks.Api.TasksFeature
{
    public class UpdateTaskRequest
    {
        public string Summary { get; set; }
    }
}
