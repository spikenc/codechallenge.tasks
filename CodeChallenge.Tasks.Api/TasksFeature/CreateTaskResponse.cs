﻿using System;
namespace CodeChallenge.Tasks.Api.TasksFeature
{
    public class CreateTaskResponse
    {
        public Guid TaskId { get; set; }
    }
}
