﻿using System;
namespace CodeChallenge.Tasks.Api.TasksFeature
{
    public class TaskResponse
    {
        public Guid TaskId { get; set; }
        public string Summary { get; set; }
        public DateTime? PerformedAt { get; set; }
        public Guid TaskOwnerId { get; set; }
        public bool IsCompleted { get; set; }
    }
}
