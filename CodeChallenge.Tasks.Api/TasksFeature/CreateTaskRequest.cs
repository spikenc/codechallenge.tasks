﻿using System;
namespace CodeChallenge.Tasks.Api.TasksFeature
{
    public class CreateTaskRequest
    {
        public string Summary { get; set; }
    }
}
