﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.TaskFeature;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CodeChallenge.Tasks.Api.TasksFeature
{
    [ApiController]
    [Route("[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ISender _sender;

        private async Task<Domain.User> CurrentUser() => await _sender.Send(new GetUsersByIdQuery { UserId = User.GetUserId() });

        public TasksController(ISender sender)
        {
            _sender = sender;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(
            int page,
            int itemsPerPage)
        {
            var currentUser = await CurrentUser();
            var tasks = await _sender.Send(new GetTasksQuery
            {
                RequestorUserId = currentUser.UserId,
                Page = page,
                ItemsPerPage = itemsPerPage
            });
            return Ok(tasks.Select(Map));
        }

        [HttpGet("{taskId}")]
        public async Task<IActionResult> Get(Guid taskId)
        {
            var currentUser = await CurrentUser();
            var task = await _sender.Send(new GetTaskQuery
            {
                TaskId = taskId,
                RequestorId = currentUser.UserId
            });

            if (task == null) return NotFound();
            return Ok(Map(task));
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateTaskRequest request)
        {
            var currentUser = await CurrentUser();
            if (currentUser.Role != Domain.UserRole.Technician) return Forbid();

            var commandResult = await _sender.Send(
                new CreateTaskCommand
                {
                    TaskCreatorId = currentUser.UserId,
                    Summary = request.Summary
                });
            if (!commandResult.ValidationResult.IsSuccessful) return BadRequest(commandResult.ValidationResult.Error);

            return CreatedAtAction(nameof(Create), new { id = commandResult.Result },
                new CreateTaskResponse
                {
                    TaskId = commandResult.Result
                });
        }

        [HttpPost("{taskId}/complete")]
        public async Task<IActionResult> CompleteTask(Guid taskId)
        {
            var currentUser = await CurrentUser();
            if (currentUser.Role != Domain.UserRole.Technician) return Forbid();

            var commandResult = await _sender.Send(
                new CompleteTaskCommand
                {
                    TaskId = taskId,
                    UserId = currentUser.UserId
                });

            if (!commandResult.ValidationResult.IsSuccessful) return BadRequest(commandResult.ValidationResult.Error);
            return NoContent();
        }

        [HttpPut("{taskId}")]
        public async Task<IActionResult> Update(Guid taskId, UpdateTaskRequest request)
        {
            var currentUser = await CurrentUser();
            if (currentUser.Role != Domain.UserRole.Technician) return Forbid();
            _ = await _sender.Send(new UpdateTaskCommand
            {
                Requestor = currentUser.UserId,
                TaskId = taskId,
                Summary = request.Summary
            });

            return NoContent();
        }

        [HttpDelete("{taskId}")]
        public async Task<IActionResult> Delete(Guid taskId)
        {
            var currentUser = await CurrentUser();
            if (currentUser.Role != Domain.UserRole.Manager) return Forbid();

            await _sender.Send(new DeleteTaskCommand
            {
                RequestorUserId = currentUser.UserId,
                TaskId = taskId
            });
            return NoContent();
        }

        private static TaskResponse Map(Domain.Task task)
        {
            return new TaskResponse
            {
                TaskId = task.TaskId,
                Summary = task.Summary,
                TaskOwnerId = task.TaskOwnerId,
                IsCompleted = task.IsCompleted,
                PerformedAt = task.CompletedAt,
            };
        }
    }
}
