﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CodeChallenge.Tasks.Api.UsersFeature
{
    public class AuthenticateRequest
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
