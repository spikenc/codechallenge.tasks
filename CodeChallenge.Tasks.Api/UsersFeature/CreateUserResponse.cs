﻿using System;

namespace CodeChallenge.Tasks.Api.UsersFeature
{
    public class CreateUserResponse
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
    }
}
