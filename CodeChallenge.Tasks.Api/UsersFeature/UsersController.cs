﻿using System;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Application.Authentication;
using CodeChallenge.Tasks.Application.UserFeature;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace CodeChallenge.Tasks.Api.UsersFeature
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ISender _sender;

        public UsersController(ISender sender)
        {
            _sender = sender;
        }

        [HttpPost("authenticate")]
        [AllowAnonymous]
        public async Task<IActionResult> Authenticate(AuthenticateRequest request)
        {
            var response = await _sender.Send(new AuthenticateCommand
            {
                Username = request.Username,
                Password = request.Password
            });
            if (response == null) return Unauthorized("Username or password is incorrect");


            var authenticationResponse = new AuthenticateResponse
            {
                Token = response.Token,
                Id = Guid.Empty
            };
            return Ok(authenticationResponse);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Create(CreateUserRequest request)
        {
            if (!Enum.TryParse(request.Role, out Domain.UserRole role)) return BadRequest("Invalid Role");

            var commandResult = await _sender.Send(new CreateUserCommand
            {
                UserName = request.Username,
                Password = request.Password,
                Name = request.Name,
                Role = role
            });

            return CreatedAtAction(nameof(Create), new { id = commandResult.Result },
                new CreateUserResponse
                {
                    UserId = commandResult.Result,
                    Username = request.Username,
                    Role = request.Role
                });
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var users = await _sender.Send(new GetUsersQuery());
            var response = users.Select(x => new CreateUserResponse
            {

                UserId = x.UserId,
                Username = x.Username,
                Role = x.Role.ToString()
            });
            return Ok(response);
        }
    }
}
