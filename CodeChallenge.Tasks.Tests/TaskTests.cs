﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Api.TasksFeature;
using CodeChallenge.Tasks.Api.UsersFeature;
using CodeChallenge.Tasks.Infrastructure.Persistence;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace CodeChallenge.Tasks.Tests
{
    [Collection(TestCollection.Integration)]
    public class TaskTests
        : IClassFixture<ApiWebApplicationFactory>,
            IAsyncLifetime
    {
        private readonly ApiWebApplicationFactory _fixture;
        private readonly HttpClient _client;
        private const string TasksResourceUrl = "/tasks";
        private const string UsersResourceUrl = "/users";
        const string ManagerRole = "Manager";
        const string TechnicianRole = "Technician";

        public TaskTests(ApiWebApplicationFactory fixture)
        {
            _fixture = fixture;
            _client = fixture.CreateClient();
        }

        public async Task InitializeAsync()
        {
            var scope = _fixture.Services.CreateScope();
            var services = scope.ServiceProvider;
            var database = services.GetRequiredService<TaskDbContext>().Database;
            await database.EnsureDeletedAsync();
            await database.EnsureCreatedAsync();
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async Task GetTasks_WhenIsManager_ReturnsTasks()
        {
            // arrange
            var technicianRequest = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = TechnicianRole,
                Password = "P@ssw0rd",
            };
            var technicianUser = await CreateUser(technicianRequest);
            var technicianAuthentication = await AuthenticateUser(new AuthenticateRequest
            {
                Username = technicianRequest.Username,
                Password = technicianRequest.Password
            });
            SetAuthenticationToken(_client, technicianAuthentication.Token);
            await CreateTask(new CreateTaskRequest
            {
                Summary = "Summary1"
            });
            await CreateTask(new CreateTaskRequest
            {
                Summary = "Summary2"
            });

            var managerRequest = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = ManagerRole,
                Password = "P@ssw0rd",
            };

            var manager = await CreateUser(managerRequest);
            var managerAuthentication = await AuthenticateUser(new AuthenticateRequest
            {
                Username = managerRequest.Username,
                Password = managerRequest.Password
            });
            SetAuthenticationToken(_client, managerAuthentication.Token);

            // act
            var response = await _client.GetAsync($"{TasksResourceUrl}?page=1&itemsPerPage=1");

            // assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var tasks = await response.Content.ReadFromJsonAsync<IEnumerable<TaskResponse>>();
            tasks.Should().NotBeNull();
            tasks.Count().Should().Be(1);
        }

        [Fact]
        public async Task GetTasks_WhenIsTechnicianAndHasCreatedTasks_ReturnsTasks()
        {
            // arrange
            var technicianRequest = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = TechnicianRole,
                Password = "P@ssw0rd",
            };
            await CreateUser(technicianRequest);
            var technicianAuthentication = await AuthenticateUser(new AuthenticateRequest
            {
                Username = technicianRequest.Username,
                Password = technicianRequest.Password
            });
            SetAuthenticationToken(_client, technicianAuthentication.Token);
            await CreateTask(new CreateTaskRequest
            {
                Summary = "Summary1"
            });
            await CreateTask(new CreateTaskRequest
            {
                Summary = "Summary2"
            });

            // act
            var response = await _client.GetAsync($"{TasksResourceUrl}?page=1&itemsPerPage=1");

            // assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var tasks = await response.Content.ReadFromJsonAsync<IEnumerable<Domain.Task>>();
            tasks.Should().NotBeNull();
            tasks.Count().Should().Be(1);
        }

        [Fact]
        public async Task GetTasks_WhenUserIsTechnicianAndHasNoTasks_ReturnsNoTasks()
        {
            // arrange
            var technicianRequest1 = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = TechnicianRole,
                Password = "P@ssw0rd",
            };
            await CreateUser(technicianRequest1);
            var technicianRequest2 = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = TechnicianRole,
                Password = "P@ssw0rd",
            };
            await CreateUser(technicianRequest2);

            var technicianAuthentication1 = await AuthenticateUser(new AuthenticateRequest
            {
                Username = technicianRequest1.Username,
                Password = technicianRequest1.Password
            });
            var technicianAuthentication2 = await AuthenticateUser(new AuthenticateRequest
            {
                Username = technicianRequest2.Username,
                Password = technicianRequest2.Password
            });
            SetAuthenticationToken(_client, technicianAuthentication1.Token);
            await CreateTask(new CreateTaskRequest
            {
                Summary = "Summary1"
            });
            await CreateTask(new CreateTaskRequest
            {
                Summary = "Summary2"
            });
            SetAuthenticationToken(_client, technicianAuthentication2.Token);

            // act
            var response = await _client.GetAsync($"{TasksResourceUrl}?page=1&itemsPerPage=1");

            // assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var tasks = await response.Content.ReadFromJsonAsync<IEnumerable<Domain.Task>>();
            tasks.Should().NotBeNull();
            tasks.Any().Should().BeFalse();
        }

        [Fact]
        public async Task CreateTask_TaskCompleted_ReturnsOk()
        {
            var technicianRequest = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = TechnicianRole,
                Password = "P@ssw0rd",
            };
            await CreateUser(technicianRequest);

            var technicianAuthentication = await AuthenticateUser(new AuthenticateRequest
            {
                Username = technicianRequest.Username,
                Password = technicianRequest.Password
            });
            SetAuthenticationToken(_client, technicianAuthentication.Token);

            var task = await CreateTask(new CreateTaskRequest
            {
                Summary = "Summary1"
            });
               
            var response = await _client.PostAsync($"{TasksResourceUrl}/{task.TaskId}/complete", null);
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);

            response = await _client.GetAsync($"{TasksResourceUrl}/{task.TaskId}");
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var newTask = await response.Content.ReadFromJsonAsync<Api.TasksFeature.TaskResponse>();
            newTask.IsCompleted.Should().BeTrue();
        }

        [Fact]
        public async Task CreateTask_WhenUserIsManager_ReturnsForbidden()
        {
            // arrange
            var managerRequest = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = ManagerRole,
                Password = "P@ssw0rd",
            };
            await CreateUser(managerRequest);
            var managerAuthentication = await AuthenticateUser(new AuthenticateRequest
            {
                Username = managerRequest.Username,
                Password = managerRequest.Password
            });
            SetAuthenticationToken(_client, managerAuthentication.Token);

            // act
            var response = await _client.PostAsJsonAsync(
                TasksResourceUrl,
                new CreateTaskRequest
                {
                    Summary = "Summary"
                });

            // assert
            response.StatusCode.Should().Be(HttpStatusCode.Forbidden);
        }

        [Fact]
        public async Task DeleteTask_WhenUserIsManager_ReturnsDeleted()
        {
            // create technician
            var technicianRequest = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = TechnicianRole,
                Password = "P@ssw0rd",
            };
            await CreateUser(technicianRequest);
            var technicianAuthentication = await AuthenticateUser(new AuthenticateRequest
            {
                Username = technicianRequest.Username,
                Password = technicianRequest.Password
            });
            SetAuthenticationToken(_client, technicianAuthentication.Token);

            var response = await _client.PostAsJsonAsync(
                TasksResourceUrl,
                new CreateTaskRequest
                {
                    Summary = "Summary"
                });

            response.StatusCode.Should().Be(HttpStatusCode.Created);
            var taskResponse = await response.Content.ReadFromJsonAsync<CreateTaskResponse>();

            // create manager
            var managerRequest = new CreateUserRequest
            {
                Username = Guid.NewGuid().ToString(),
                Name = "John",
                Role = ManagerRole,
                Password = "P@ssw0rd",
            };
            await CreateUser(managerRequest);
            var managerAuthentication = await AuthenticateUser(new AuthenticateRequest
            {
                Username = managerRequest.Username,
                Password = managerRequest.Password
            });
            SetAuthenticationToken(_client, managerAuthentication.Token);

            // delete task

            response = await _client.DeleteAsync($"{TasksResourceUrl}/{taskResponse.TaskId}");
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        //[Fact]
        //public async Task DeleteTask_WhenUserIsTechician_ReturnsBadRequest()
        //{
        //    //Create tech
        //    // create task
        //    //deletes task
        //}

        private void DeleteTaskUseCase()
        {

        }

        private async Task<CreateUserResponse> CreateUser(CreateUserRequest createUserRequest)
        {
            var response = await _client.PostAsJsonAsync(
                UsersResourceUrl,
                createUserRequest);
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            var user = await response.Content.ReadFromJsonAsync<CreateUserResponse>();
            return user;
        }

        private async Task<AuthenticateResponse> AuthenticateUser(AuthenticateRequest authenticateRequest)
        {
            var response = await _client.PostAsJsonAsync(
                "users/authenticate",
                authenticateRequest);
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            return await response.Content.ReadFromJsonAsync<AuthenticateResponse>();
        }

        private void SetAuthenticationToken(HttpClient client,  string token)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        private async Task<CreateTaskResponse> CreateTask(CreateTaskRequest taskRequest)
        {
            var response = await _client.PostAsJsonAsync(
                TasksResourceUrl,
                taskRequest);

            response.StatusCode.Should().Be(HttpStatusCode.Created);
            return await response.Content.ReadFromJsonAsync<CreateTaskResponse>();
        }
    }
}