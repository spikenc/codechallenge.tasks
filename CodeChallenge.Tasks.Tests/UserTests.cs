﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using CodeChallenge.Tasks.Api.UsersFeature;
using CodeChallenge.Tasks.Infrastructure.Persistence;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace CodeChallenge.Tasks.Tests
{
    [Collection(TestCollection.Integration)]
    public class UserTests
         : IClassFixture<ApiWebApplicationFactory>,
             IAsyncLifetime
    {
        private readonly ApiWebApplicationFactory _fixture;
        private readonly HttpClient _client;
        private const string UsersResourceUrl = "/users";

        public UserTests(ApiWebApplicationFactory fixture)
        {
            _fixture = fixture;
            _client = fixture.CreateClient();
        }

        public async Task InitializeAsync()
        {
            var scope = _fixture.Services.CreateScope();
            var services = scope.ServiceProvider;
            var database = services.GetRequiredService<TaskDbContext>().Database;
            await database.EnsureDeletedAsync();
            await database.EnsureCreatedAsync();
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async Task CreateUser_WhenUserDoesNotExists_ReturnsOK()
        {
            var response = await _client.PostAsJsonAsync(
                UsersResourceUrl,
                new CreateUserRequest
                {
                    Name = "John",
                    Role = "Manager",
                    Password = "P@ssw0rd",
                    Username = "john@task.io"
                });
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            var user = await response.Content.ReadFromJsonAsync<CreateUserResponse>();
            user.Should().NotBeNull();
            user.UserId.Should().NotBe(Guid.Empty);
        }

        [Fact]
        public async Task CreateUser_WhenUserHasInvalidRole_ReturnsBadRequest()
        {
            var response = await _client.PostAsJsonAsync(
                UsersResourceUrl,
                new CreateUserRequest
                {
                    Name = "Mary",
                    Role = "Invalid",
                    Password = "P@ssw0rd",
                    Username = "mary@task.io"
                });
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}
