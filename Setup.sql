Build started...
Build succeeded.
CREATE TABLE IF NOT EXISTS `__EFMigrationsHistory` (
    `MigrationId` varchar(150) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    PRIMARY KEY (`MigrationId`)
);

START TRANSACTION;

CREATE TABLE `TaskNotifications` (
    `TaskId` varbinary(16) NOT NULL,
    `CompletedAt` datetime NOT NULL,
    PRIMARY KEY (`TaskId`)
);

CREATE TABLE `Tasks` (
    `TaskId` varbinary(16) NOT NULL,
    `Summary` varchar(2500) NOT NULL,
    `TaskOwnerId` varbinary(16) NOT NULL,
    `IsCompleted` tinyint(1) NOT NULL,
    `CompletedAt` datetime NULL,
    PRIMARY KEY (`TaskId`)
);

CREATE TABLE `Users` (
    `UserId` varbinary(16) NOT NULL,
    `Username` varchar(512) NOT NULL,
    `Name` varchar(512) NULL,
    `Role` int NOT NULL,
    `PasswordHash` text NULL,
    PRIMARY KEY (`UserId`),
    CONSTRAINT `AK_Users_Username` UNIQUE (`Username`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20210815192650_Init', '5.0.9');

COMMIT;


